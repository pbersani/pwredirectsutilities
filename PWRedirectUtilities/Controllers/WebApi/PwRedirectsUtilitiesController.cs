﻿using System;
using System.Linq;
using System.Web.Http;
using Umbraco.Core;
using Umbraco.Core.Configuration;
using Umbraco.Web.WebApi;

namespace PWRedirectUtilities.Controllers.WebApi
{
    public class PwRedirectsUtilitiesController : UmbracoAuthorizedApiController
    {
        [HttpGet]
        public IHttpActionResult Test()
        {
            return Ok();
        }

        /// <summary>
        /// Registers a new redirect with the RedirectUrlService
        /// </summary>
        /// <param name="originalUrl">string value that is a Url segment.  It could be a fully qualified host name or relative path.</param>
        /// <param name="contentKey">Guid key for the Content in Umbraco that the redirect with point to.</param>
        /// <returns></returns>
        [HttpGet]
        public IHttpActionResult AddRedirectUrl(string originalUrl, string contentKey)
        {
            var contentService = Services.ContentService;
            var redirectUrlService = Services.RedirectUrlService;
            var contentGuid = Guid.Parse(contentKey);

            try
            {
                var fromUrl = OriginalUrlTransformForRoot(originalUrl);
                redirectUrlService.Register(fromUrl, contentGuid);

                // Needed to fix an issue with Courier where the redirect created makes Courier think there is no property data.
                // This results in the live site ends up with all properties being empty.  The fix is to save / publish then the
                // properties are detected and Courier works as expected.
                //
                // See issue here: http://issues.umbraco.org/issue/COU-532
                var toNode = contentService.GetById(contentGuid);
                contentService.SaveAndPublishWithStatus(toNode);
                return Ok();
            }
            catch (Exception ex)
            {
                ApplicationContext.ProfilingLogger.Logger.Error(typeof(PwRedirectsUtilitiesController), "There was an unexpected problem adding a redirect using the PwRedirectUtiltieies", ex);
                throw ex;
            }
        }

        private string OriginalUrlTransformForRoot(string url)
        {
            var formattedUrl = url?.TrimStart('/');
            var firstUrlName = formattedUrl?.Split('/').FirstOrDefault();
            if (String.IsNullOrEmpty(formattedUrl) || firstUrlName == "http:" || firstUrlName == "https:")
            {
                throw new ArgumentException("Only relative URLs supported.  Please remove http/https and domain.");
            }

            var root = Umbraco.TypedContentAtRoot().Where(x => x.UrlName == firstUrlName).ToList();
            if (!root.Any())
            {
                // Cannot throw unless we can verify that the root *should* have its urlName in the full URL.
                //throw new ArgumentException("Please use the full relative URL.  Error locating the root page in Umbraco.");

                return $"/{formattedUrl}";
            }

            var rootContent = root.First();
            var transformed = formattedUrl.Replace(firstUrlName, rootContent.Id.ToString());
            return transformed;
        }
    }
}